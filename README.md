# Ressources pour le workshop Ph0wn "Hacking Retro Architectures"

- [Un article sur le développement de Ms. Pacman](https://www.gameinformer.com/b/features/archive/2015/11/03/inside-the-development-of-ms-pac_2d00_man.aspx)
- [Un autre article pour expliquer le hack Ms. Pacman](https://www.polygon.com/2016/3/25/11287572/ms-pac-man-story)
- [Le brevet déposé par General Computer Corp. pour la protection de Ms. Pacman](https://patents.google.com/patent/US4525599A/en)
- [Un désassemblage commenté de Pacman / Ms. Pacman](https://github.com/BleuLlama/GameDocs/blob/master/disassemble/mspac.asm)
- Wikipedia : [raster scan](https://en.wikipedia.org/wiki/Raster_scan)
- YouTube : [conflit d’accès à la mémoire vidéo](https://www.youtube.com/watch?v=JpkK_0AL0Cg)
- YouTube : [interaction modification d’image et balayage](https://www.youtube.com/watch?v=rg_0D96ZGSY)
- MAME : [documentation, binaires et sources](https://docs.mamedev.org/)
- [Video-games information & manuals](https://www.gamesdatabase.org/)
- Wikipedia : [video game graphics](https://en.wikipedia.org/wiki/Video_game_graphics)
- Wikipedia : [sprite](https://en.wikipedia.org/wiki/Sprite_(computer_graphics))
- Wikipedia : [ROM Hacking](https://en.wikipedia.org/wiki/ROM_hacking)
- Émulateurs SNES :
    -   [bsnes (=> higan => ares)](https://github.com/bsnes-emu/bsnes)
    -   [higan](https://en.wikipedia.org/wiki/Higan_(emulator))
    -   [Snes9x](https://en.wikipedia.org/wiki/Snes9x)
    -   [Zsnes](https://en.wikipedia.org/wiki/ZSNES) …
- Éditeurs Hexa avec différenciation de deux binaires : VbinDiff, vimdiff+hd, dhex…
- [« On the essence of ROM hacking » by InVerse](https://docs.google.com/document/d/1VTyOyRk4OL8Oay0CqXHGS8FIse-4wsFmLNPQjT58Hgk/edit)
- [SNESdev Wiki](https://snes.nesdev.org/wiki/SNESdev_Wiki)
- [SNES Development Manuals](https://archive.org/details/SNESDevManual/book1/mode/2up)
- [nocash SNES hardware specs](https://problemkaputt.de/fullsnes.htm)

